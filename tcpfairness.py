#!/usr/bin/env python
"""
Created on September 1, 2016

@author: aousterh
"""

from subprocess import check_output, STDOUT

from mininet.cli import CLI
from mininet.node import Host, Node
from mininet.link import Intf, Link, TCIntf
from mininet.log import debug, error, setLogLevel
from mininet.net import Mininet
from mininet.nodelib import LinuxBridge
from mininet.topo import Topo

class CustomMininet(Mininet):
    """A Mininet with some custom features."""

    def addSwitch(self, name, cls=None, **params):
        sw = super(CustomMininet, self).addSwitch(name, cls, **params)

        if sw.inNamespace:
            # add a loopback interface so that we can use xterm with switches
            # in their own namespaces
            sw.cmd('ifconfig lo up')

        return sw

class BasicIntf(TCIntf):
    """An interface with TSO and GSO disabled."""

    def config(self, **params):
        result = super(BasicIntf, self).config(**params)

        self.cmd('ethtool -K %s tso off gso off' % self)

        return result

class PIEIntf(BasicIntf):
    """An interface that runs the Proportional Integral controller-Enhanced AQM
    Algorithm. See the man page for info about paramaters:
    http://man7.org/linux/man-pages/man8/tc-pie.8.html."""

    def config(self, limit=1000, target="20ms", **params):
        result = super(PIEIntf, self).config(**params)

        cmd = ('%s qdisc add dev %s' + result['parent'] + 'handle 11: pie' +
               ' limit ' + str(limit) + ' target ' + target)
        parent = ' parent 11:1 '

        debug("adding pie w/cmd: %s\n" % cmd)
        tcoutput = self.tc(cmd)
        if tcoutput != '':
            error("*** Error: %s" % tcoutput)
        debug("cmd:", cmd, '\n')
        debug("output:", tcoutput, '\n')
        result['tcoutputs'].append(tcoutput)
        result['parent'] = parent
        
        return result

class AQMLink(Link):
    """A link that runs an AQM scheme on 0-2 of its interfaces."""

    def __init__(self, node1, node2, port1=None, port2=None, intfName1=None,
                 intfName2=None, cls1=TCIntf, cls2=TCIntf, **params):
        super(AQMLink, self).__init__(node1, node2, port1=port1, port2=port2,
                                      intfName1=intfName1, intfName2=intfName2,
                                      cls1=cls1, cls2=cls2, params1=params,
                                      params2=params)

class TwoSwitchTopo(Topo):
    """Topology with two switches. Switch 1 has 3 hosts, switch 2 has 2 hosts,
    switches are connected with a single link."""

    def __init__(self, bw_mbps=100, delay="10ms", max_queue_size=1000,
                 **kwargs):
        super(TwoSwitchTopo, self).__init__(**kwargs)

        link_params = {"bw": bw_mbps, "delay": delay,
                       "max_queue_size": max_queue_size}

        s1 = self.addSwitch('s1', inNamespace=True)
        s2 = self.addSwitch('s2', inNamespace=True)

        # TODO: create hosts and add appropriate links between hosts and switches
        # TODO: create link between switches, notice that the delay is different
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        h4 = self.addHost('h4')
        h5 = self.addHost('h5')

        link1 = self.addLink(h5,s2, bw=100, delay='10ms',cls1=BasicIntf, cls2=PIEIntf)
        link2 = self.addLink(h4,s2, bw=100, delay='10ms',cls1=BasicIntf, cls2=PIEIntf)

        link3 = self.addLink(s2,s1, bw=100, delay='30ms',cls1=PIEIntf, cls2=PIEIntf)

        link4 = self.addLink(h1,s1, bw=100, delay='10ms',cls1=BasicIntf, cls2=PIEIntf)
        link5 = self.addLink(h2,s1, bw=100, delay='10ms',cls1=BasicIntf, cls2=PIEIntf)
        link6 = self.addLink(h3,s1, bw=100, delay='10ms',cls1=BasicIntf, cls2=PIEIntf)


	
	# Hint: you can use **link_params to pass parameters when creating links
	# Hint: remember to enable PIE for these links: for links between hosts and
	# switches, mimic bufferbloat.py, PIE should sit on the switch side; for the
	# link between switches, enable PIE on both sides.

def set_congestion_control(cc="reno"):
    res = check_output(["sysctl", "-w",
                        "net.ipv4.tcp_congestion_control=" + cc],
                       stderr=STDOUT)
    if res != ("net.ipv4.tcp_congestion_control = %s\n" % cc):
        print "Error setting congestion control to %s. Exiting." % cc
        exit()
        
def main():
    set_congestion_control("reno")

    setLogLevel('info') # set to 'debug' for more logging

    topo = TwoSwitchTopo()
    net = CustomMininet(topo=topo, switch=LinuxBridge, controller=None,
                        xterms=True, link=AQMLink)
    fol = 'ps2-scen1'
    time = 200
    
    net.start()
    
    net.pingAll()


    # First, use CLI to explore this setting, i.e. run iperf on multiple hosts and run ping
    # Note: It can take a while to pop out xterm windows in GCP.
    #CLI(net)
    
    # TODO: Then, comment out CLI(net) and mimic bufferbloat.py, automate what you did in CLI, 
    # such that running aqm.py will output all the delivers in the problem.
    # Note: Turn off the xterm in CustomMininet by setting xterms=False
    # Let each experiment run for 200 seconds at least
    # When starting off another experiment, remember to initialize net again by doing net = ...
    # Hint: "iperf -c ... -P 10 | grep SUM" gives aggregation information of 10 flows
    # "iperf -c -i 1 ..." gives a measurement every 1 second
    # Instead of tcpprobe, you can use "iperf -c ... > txtfile" to store the results.
    
    #Scenario 1:
    #Start 10 long lived TCP flows sending data from h2 to h1, and similarly 10 long lived TCP 
    #flows from h5 to h1.
    h1 = net.getNodeByName('h1')
    h2 = net.getNodeByName('h2')
    h5 = net.getNodeByName('h5')
    server = h1.popen("iperf -s -w 16m")
    IP1 = h1.IP()
    h2.popen("iperf -c %s -t %s -P | grep SUM > %s/iperf.out" % (IP1, time, fol), shell=True)
    h5.popen("iperf -c %s -t %s -P | grep SUM > %s/iperf.out" % (IP1, time, fol), shell=True)
    #Start back-to-back ping trains from h2 to h1, and h5 to h1. Record the RTTs with ping 10 
    #times a second.
    h2.popen("ping -i 0.1 %s > %s/ping.txt" % (IP1, fol), shell=True)
    h5.popen("ping -i 0.1 %s > %s/ping.txt" % (IP1, fol), shell=True)
    #Measure the following:
        #The average throughput for h2->h1 and h5->h1 flows. For each group, measure the aggregate 
        #throughput of the 10 flows.
            #throughput = congestion window / RTT.
        #The average RTT for h2->h1 and h5->h1 flows.
    net.stop()
    #Scenario 2:
    #Now start 10 long lived TCP flows from h4 to h3.
    #Repeat the above measurements (average throughput and RTT) for the three groups of flows: h2->h1, h5->h1, and h4->h3.
    net = CustomMininet(topo=topo, switch=LinuxBridge, controller=None,
                        xterms=True, link=AQMLink)
                        
    net.stop()
    
def get_average(data):
    return sum(data)/len(data)

def get_std_dev(data):
    average = get_average(data)
    diff = []
    for d in data:
        diff.append( (d-average)**2 )
    return math.sqrt(get_average(diff))
    
if __name__ == '__main__':
    main()